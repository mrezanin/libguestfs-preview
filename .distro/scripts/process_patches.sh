TARBALL=$1
shift
SPECNAME=$1
shift
MARKER=$1
shift
LOCALVERSION=$1
shift

SCRIPTS=scripts
SOURCES=rpmbuild/SOURCES
SRPMDIR=rpmbuild/SRPM
SPEC=rpmbuild/SPECS/${SPECNAME}

# Pre-cleaning
rm -rf .tmp asection psection

cp ${TARBALL} ${SOURCES}/${TARBALL}

# Handle patches
num=0
for patchfile in ${SOURCES}/*.patch; do
  mv -f ${patchfile} .tmp
  if grep -q '^diff --git ' .tmp; then
    patchname=$(grep -x "Patch-name: .*\.patch" .tmp | sed 's/Patch-name: \(.*\)/\1/')
    patchid=$(grep -x "Patch-id: .*" .tmp | sed 's/Patch-id: \(.*\)/\1/')
    inspec=$(grep -x "Patch-present-in-specfile: .*" .tmp | sed 's/Patch-present-in-specfile: \(.*\)/\1/')
    ignore=$(grep -x "Ignore-patch: .*" .tmp | sed 's/Ignore-patch: \(.*\)/\1/')

    if [ "$ignore" == "True" ]; then
        continue
    fi

    if [ -n "$patchname" ]; then
        mv .tmp ${SOURCES}/${patchname}
        patchfile="$patchname"
    else
        patchfile=$(basename "$patchfile")
        mv .tmp ${SOURCES}/${patchfile}
    fi
    if [ -n "$patchid" ]; then
        if [ "$patchid" -gt "$num" ]; then
            num=$patchid
        fi
    else
        let num=num+1
    fi
    if [ "$inspec" != "True" ]; then
        echo "Patch${num}: ${patchfile}" >> psection
        echo "%patch${num} -p2" >> asection
    fi
  fi
done

cp ${SPECNAME} ${SPEC}
lp=$(grep -e "Patch[0-9]*:.*" ${SPEC} | tail -n 1)
if [ -z "$lp" ]; then
  lp=$(grep -e "Source[0-9]:.*" ${SPEC} | tail -n 1)
fi

sed -i "/$lp/r psection" ${SPEC}
sed -i '/# Apply source-git patches/r asection' ${SPEC}
if [ -n "${LOCALVERSION}" ]; then
  sed -i "s/\(^Release:.*\)/\1\.${LOCALVERSION}/" ${SPEC}
fi

# Post-cleaning
rm -rf .tmp asection psection
